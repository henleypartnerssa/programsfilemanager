<?php

Route::get("/users/filesystem/upload-quotations/{file_name}", "hpsadev\ProgramsFileManager\Controllers\FileManagerController@uploadPdfToQuotations");
Route::get("/users/filesystem/upload-payments/{file_name}", "hpsadev\ProgramsFileManager\Controllers\FileManagerController@uploadPdfToPayments");
Route::get("/users/filesystem/upload-receipts/{file_name}", "hpsadev\ProgramsFileManager\Controllers\FileManagerController@uploadPdfToReceipts");

Route::post("/users/filesystem/upload-profile-uploads", "hpsadev\ProgramsFileManager\Controllers\FileManagerController@uploadToProfileUploads");
Route::post("/users/filesystem/delete-profile-uploads", "hpsadev\ProgramsFileManager\Controllers\FileManagerController@deleteFromUploads");
Route::get("/users/filesystem/download-profile-uploads/{fileName}", "hpsadev\ProgramsFileManager\Controllers\FileManagerController@downloadFromUploads");

Route::post("/users/filesystem/upload-profile-notary", "hpsadev\ProgramsFileManager\Controllers\FileManagerController@uploadToProfileNotary");
Route::post("/users/filesystem/delete-profile-notary", "hpsadev\ProgramsFileManager\Controllers\FileManagerController@deleteFromNotary");
Route::get("/users/filesystem/download-profile-notary/{fileName}", "hpsadev\ProgramsFileManager\Controllers\FileManagerController@downloadFromNotary");

Route::post("/users/filesystem/generate-government-form", "hpsadev\ProgramsFileManager\Controllers\FileManagerController@generateGovForm");
Route::post("/users/filesystem/delete-profile-downloads", "hpsadev\ProgramsFileManager\Controllers\FileManagerController@deleteGovForm");
Route::get("/users/filesystem/download-profile-downloads/{fileName}", "hpsadev\ProgramsFileManager\Controllers\FileManagerController@downloadGovForm");

Route::post("/users/filesystem/upload-profile-payments", "hpsadev\ProgramsFileManager\Controllers\FileManagerController@uploadToProfilePayments");
Route::post("/users/filesystem/delete-profile-payments", "hpsadev\ProgramsFileManager\Controllers\FileManagerController@deleteFromPayments");
Route::get("/users/filesystem/download-profile-payments/{fileName}", "hpsadev\ProgramsFileManager\Controllers\FileManagerController@downloadFromPayments");

Route::post("/users/filesystem/upload-profile-receipts", "hpsadev\ProgramsFileManager\Controllers\FileManagerController@uploadToProfileReceipts");
Route::post("/users/filesystem/delete-profile-receipts", "hpsadev\ProgramsFileManager\Controllers\FileManagerController@deleteFromReceipts");
Route::get("/users/filesystem/download-profile-receipts/{fileName}", "hpsadev\ProgramsFileManager\Controllers\FileManagerController@downloadFromReceipts");

Route::get("/users/filesystem/list-documents/{sortKey}", "hpsadev\ProgramsFileManager\Controllers\FileManagerController@listDocumentes");
