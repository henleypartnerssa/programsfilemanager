<?php

namespace hpsadev\ProgramsFileManager;

use Storage;
use Auth;
use App\User;
use App\Profile;

/**
 * Application Directory Manger. Creates and lists all User
 * and Profile Directories.
 *
 * @author      Gershon Koks <gershon.koks@henleyglobal.com>
 * @copyright   Henley & Partners Ltd
 * @license     For Henley & Partners use only.
 * @version     1.0
 */
class DirectoryManager {

    private $userId     = null;
    private $profileId  = null;
    private $usersDir   = null;

    public function __construct() {
        $this->validateUsersDir();
    }

    /**
     * Validates and Creates core User Dir.
     * @since  1.0 Function Init
     */
    private function validateUsersDir() {
        $usersDirs = Storage::directories();

        if (count($usersDirs) === 0) {
            Storage::makeDirectory('users');
        }
    }

    /**
     * Creates User Base Dir
     * @since  1.0 Function Init
     * @param  integer $userId UserID
     */
    public function createUserBaseDirs($userId) {
        $this->usersDir = '/users/'.$userId . '/';
        Storage::makeDirectory($this->usersDir);
        $this->createBaseDirs();
    }

    /**
     * Executes creation of User Base Dir.
     * @since  1.0 Function Init
     */
    private function createBaseDirs() {
        Storage::makeDirectory($this->usersDir.'quotations');
        Storage::makeDirectory($this->usersDir.'payments');
        Storage::makeDirectory($this->usersDir.'receipts');
        Storage::makeDirectory($this->usersDir.'profiles');
        Storage::makeDirectory($this->usersDir.'approvals');
    }

    /**
     * Create Profile + subdirs for given Profile.
     * @since  1.0 Function Init
     * @param  integer $userId    Auth User ID
     * @param  integer $profileID Profile ID would dirs should be created for.
     */
    public function createProfileDir($profileID) {
        $profile        = Profile::find($profileID);
        $identifier     = (config('programsfilemanager.profileIdentifier') !== 'ID') ? $profile->first_name : $profileID;
        $profilesDir    = '/users/'.Auth::id(). '/profiles/'.$identifier;
        Storage::makeDirectory($profilesDir);
        Storage::makeDirectory($profilesDir.'/uploads');
        Storage::makeDirectory($profilesDir.'/downloads');
        Storage::makeDirectory($profilesDir.'/notary');
    }

    /**
     * Static Function which Deletes the Given Profile ID
     * from Disk.
     * @since  1.0 Function Init
     * @param  integer $profileID ProfileID
     */
    public static function delProfileDir($profileID) {
        $profilesDir = storage_path().'/users/'.Auth::id(). '/profiles/'.$profileID;
        exec('rm -r -f '.$profilesDir);
    }

    /**
     * Get Upper Layer user Directory Files.
     * @since  1.0 Function Init
     * @param  string $dir  directoy/path
     * @return array        files
     */
    public static function getUpperFilesByDir($dir) {
        $path       = storage_path().'/app/users/'.Auth::id().'/'.$dir.'/';
        $dirFiles   = scandir($path);
        $files      = [];
        foreach($dirFiles as $file) {
            if ($file === '.' || $file === '..') {
                continue;
            }
            array_push($files, $file);
        }
        return $files;
    }

}
