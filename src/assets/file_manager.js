/**
 * File Manager JS Code for Handling file uploads
 * @version 0.0.3
 */

$('document').ready(function() {

    FileUploader = function FileUploader(documentType, keyIdentifier, folder) {
        this.docType    = documentType;
        this.keyID      = keyIdentifier;
        this.folder     = folder;
        this.counter    = Number($('#total_'+keyIdentifier).val());

        this.generateFileInput = function(type) {
           ++this.counter;
           var code = this.keyID+"-"+this.counter;
           var obj = {
               element : '<input type="file" name="'+code+'" id="'+code+'">',
               code    : code
           };
           return obj;
       }

       this.addToList = function(type, obj) {
           var _this = this;
           $('#'+obj.code).on('change', function(){
               $('.ui.'+type+'.list').append(_this.htmlDep(type, obj.code, $('#'+obj.code)[0].files[0].name));
               _this.uploadFileEvnt(obj.code,type);
           });
           $('#'+obj.code).trigger('change');
       }

        this.uploadFile = function() {
            var _this = this;
            $('#add_'+this.keyID).on('click', function(event){
                event.preventDefault();
                var obj = _this.generateFileInput(_this.keyID);
                $('#file_inputs').append(obj.element);
                $('#'+obj.code).trigger('click');
                _this.addToList(_this.keyID, obj);
            });
        }

        this.uploadFileEvnt = function(code,type) {
            var _this = this;

            $('#button-'+code).on('click', function() {
                var url = "/users/filesystem/upload-profile-"+_this.folder;
                $('#button-'+code).addClass('disabled');
                $('#button-'+code).addClass('loading');
                $('#'+code).upload(url, {
                    _token      : $('#token').val(),
                    profile_id  : $('#profile_id').val()
                },
                function(success) {
                    if (success !== 'failed') {
                        var newText = "File Name: ".concat(success);
                        $('#button-'+code).addClass('delete');
                        $('#button-'+code).removeClass('blue');
                        $('#button-'+code).addClass('green');
                        $('#button-'+code).text('Delete');
                        $('#button-'+code).unbind('click');
                        $('#button-'+code).removeClass('disabled');
                        $('#button-'+code).removeClass('loading');
                        $('.content.'+code).text(newText);
                        $('#button-'+code).attr('data-fileName', success);
                        $('#total_'+_this.keyID).val(_this.counter);
                        _this.deleteFile();
                    } else {
                        $('#button-'+code).addClass('error');
                        $('#button-'+code).removeClass('blue');
                        $('#button-'+code).addClass('red');
                        $('#button-'+code).text('Delete - Error File');
                        $('#button-'+code).unbind('click');
                        $('#button-'+code).removeClass('loading');
                        $('#button-'+code).removeClass('disabled');

                        $('.ui.button.error.red').on('click', function() {
                            var id = $(this).attr('id');
                            var n = id.replace("button-", "");
                            $('.item.'+n).remove();
                        });
                    }
                },
                function(prog, value) {
                });
            });
        }

        this.deleteFile = function() {
            var _this = this;
            $('.ui.button.delete').on('click', function() {
                if (_this.folder === '') {
                    _this.folder = $(this).data('folder');
                }
                console.log(_this.folder);
                var id      = $(this).attr('id');
                var url     = '/users/filesystem/delete-profile-'+_this.folder;
                var newId   = id.replace('button-', '');
                var file    = $(this).data('filename');
                _this.minus(newId);
                $.ajax({
                    type: 'POST',
                    url: url,
                    async : true,
                    data: {
                        _token      : $('#token').val(),
                        file        : file,
                        profile_id  : $('#profile_id').val(),
                        id          : newId
                    },
                    success: function(postData) {
                        if (postData !== 'error') {
                            $('.item.'+postData).hide('slow', function() { $(this).remove(); });
                            $('#'+newId).remove();
                        }
                    }
                });
            });
        }

        this.minus = function(code) {
            if (code.indexOf(this.keyID) >= 0) {
                --this.counter;
                $('#total_'+this.keyID).val(this.counter);
            }
        }

        this.htmlDep = function(type, code, name) {
            var sect1   = '<div class="item ' +code+ '"><div class="right floated content">';
            var sect2   = '<div class="ui blue button" id="button-'+code+'">Upload</div></div>';
            var sect3   = '<i class="ui avatar image"><i class="red large file pdf outline icon"></i></i><div class="content ' +code+ '">File Name: ' + name + '</div></div>';
            var html    = sect1 + sect2 + sect3;
            return html;
        }

    }

});
