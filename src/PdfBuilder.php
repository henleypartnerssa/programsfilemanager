<?php

namespace hpsadev\ProgramsFileManager;

use Auth;
use Crypt;
use hpsadev\ProgramsFileManager\Mappers\D1Mapper;
use hpsadev\ProgramsFileManager\Mappers\D2Mapper;
use hpsadev\ProgramsFileManager\Mappers\D3Mapper;
use hpsadev\ProgramsFileManager\Mappers\D4Mapper;
use hpsadev\ProgramsFileManager\Mappers\PassportMapper;

/**
 * HP Online Applications PDF Builder used to generate
 * and populate Governemnt Forms.
 *
 * @author      Gershon Koks <gershon.koks@henleyglobal.com>
 * @copyright   Henley & Partners Ltd
 * @license     For Henley & Partners use only.
 * @version     1.1
 */
class PdfBuilder {

    private $user;
    private $profile;
    private $template;
    private $type;
    private $output_file;
    private $path;
    private $download_path;
    private $fields;
    private $jar_file;
    private $command;

    /**
     * Class Constructor
     *
     * @param string $formType Specified PDF Form type.
     * @param profile $profile  Profile PDF should be built for.
     */
    public function __construct($formType = NULL, $profile = NULL) {
        $this->profile  = $profile;
        $this->user     = $profile->user;
        $this->type     = $formType;
        $this->setJarFile();
        if ($profile) {
            $this->setTemplate($formType);
            $this->setDownloadPath();
            $this->setOutputFile();
            $this->map();
        }
    }

    /**
     * Sets the PDF template.
     * @since   1.0   Function Init
     * @param string $formType PDF Form Type
     */
    public function setTemplate($formType) {
        $baseTempPath   = __DIR__."/Templates/";
        $this->template = $baseTempPath.$formType.'.pdf';
    }

    /**
     * Internal Method that sets the Profile Download Path.
     * @since   1.0   Function Init
     */
    private function setDownloadPath() {
        $this->download_path = storage_path("/app/users/".$this->user->user_id."/profiles/".$this->profile->profile_id."/downloads//");
    }

    /**
     * Sets the Built Output File and Path or Ouput Path
     * and User defined output file name.
     * @since   1.0   Function Init
     * @param string $customName User Defined File Name.
     */
    public function setOutputFile($customName = null) {
        if ($customName) {
            $this->output_file = $this->download_path.$customName;
        } else {
            $this->output_file = $this->download_path.$this->profile->first_name.'-'.$this->profile->last_name.'-'.$this->type.'.pdf';
        }
    }

    public function getOutputFileName() {
        return $this->profile->first_name.'-'.$this->profile->last_name.'-'.$this->type.'.pdf';
    }

    /**
     * Sets the Java JAR file.
     * @since   1.0   Function Init
     * @param string $customJarFile User defined JAR file.
     */
    public function setJarFile($customJarFile = null) {
        if ($customJarFile) {
            $this->jar_file = $customJarFile;
        } else {
            $this->jar_file = __DIR__.'/java/pdflib/HD-PDFLib.jar';
        }
    }

    /**
     * Executes JAVA command.
     *
     * @return string indicator 1/0
     */
    private function execute($jsonTicket) {
        $cmd = "java -jar ".$this->jar_file.' '.json_encode(json_encode($jsonTicket, JSON_UNESCAPED_SLASHES), JSON_UNESCAPED_SLASHES);
        //dd($cmd);
        $exe = shell_exec($cmd);
        $this->cryptFile($this->output_file);
        return $exe;
    }

    /**
     * PDF data fields Mapper.
     * @since   1.1   Function Init
     * @param  string $fieldName  Field Name
     * @param  string $fieldValue Field Value
     */
    private function map() {
        if ($this->type === 'D1') {
            $d1 = new D1Mapper($this->profile);
            $this->fields = $d1->getFields();
        } else if ($this->type === 'D2') {
            $d2 = new D2Mapper($this->profile);
            $this->fields = $d2->getFields();
        } else if ($this->type === 'D3') {
            $d3 = new D3Mapper($this->profile);
            $this->fields = $d3->getFields();
        } else if ($this->type === 'D4') {
            $d4 = new D4Mapper($this->profile);
            $this->fields = $d4->getFields();
        } else if ($this->type === 'Passport') {
            $passport = new PassportMapper($this->profile);
            $this->fields = $passport->getFields();
        }
    }

    /**
     * Build the PDF File.
     * @since   1.1   Function Init
     * @return string indicator 1/0
     */
    public function build() {
        $ticket['type']         = $this->type;
        $ticket['template']     = $this->template;
        $ticket['dataFilePath'] = $this->output_file;
        $ticket['fields']       = $this->fields;
        shell_exec('chmod 777 '.$this->output_file);
        return $this->execute($ticket);
    }

    /**
     * Passport Encrypts Uploaded PDF.
     * @since   1.1   Function Init
     * @param  string $file Full file name of file to be encrypted.
     */
    public function encryptUpload($file) {
        $this->cryptFile($file);
    }

    /**
     * Passport Dencrypts Uploaded PDF.
     * @since   1.1   Function Init
     * @param  string $file Full file name of file to be dencrypted.
     */
    public function decryptDownload($file) {
        $this->decryptFile($file);
    }

    /**
     * Admin passport Encrypts Uploaded PDF.
     * @since   1.1   Function Init
     * @param string $file Full file name of file to be encrypted.
     */
    public function encryptForAdmin($file) {
        $this->decryptFile($file);
        $pas = Crypt::decrypt(Auth::user()->doc_pass);
        $new = "'" . $pas . "'";
        $jar = __DIR__.'/java/pdflib/pdfbox-app-2.0.2.jar';
        $cmd = 'java -jar '.$jar.' Encrypt -U '.$new.' '.$file;
        $exe = shell_exec($cmd);
    }

    /**
     * Executes Encryption shell command.
     * @since   1.1   Function Init
     * @param  string $file Full file name of file to be encrypted.
     */
    private function cryptFile($file) {
        shell_exec('chmod 777 '.$file);
        $pas = Crypt::decrypt($this->user->doc_pass);
        $new = "'" . $pas . "'";
        $jar = __DIR__.'/java/pdflib/pdfbox-app-2.0.2.jar';
        $cmd = 'java -jar '.$jar.' Encrypt -U '.$new.' '.$file;
        $exe = shell_exec($cmd);
    }

    /**
     * Executes Dencryption shell command.
     * @since   1.1   Function Init
     * @param  string $file Full file name of file to be dencrypted.
     */
    private function decryptFile($file) {
        shell_exec('chmod 777 '.$file);
        $pas = Crypt::decrypt($this->user->doc_pass);
        $new = "'" . $pas . "'";
        $jar = __DIR__.'/java/pdflib/pdfbox-app-2.0.2.jar';
        $cmd = 'java -jar '.$jar.' Decrypt -password '.$new.' '.$file;
        $exe = shell_exec($cmd);
    }

}
