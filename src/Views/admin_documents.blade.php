@extends('nladminpanel::app_left')

@section('title', 'Documents')

@section('left')
    @include('nladminpanel::_left', $data = ['link' => 'doc'])
@stop

@section('content')

<h3 class="ui center aligned block header">{{$profile->first_name}} {{$profile->last_name}} - Documents</h3>

<div class="ui stacked segment">
    <div class="ui grid">
        <div class="row">
            <div class="sixteen wide column">
                <div class="ui tabular menu">
                    @foreach($user->profiles()->orderBy('created_at')->get() as $p)
                    <a class="item {{($p->profile_id === (int)$profile_id) ? 'active' : ''}}" href="/users/filesystem/list-documents/{{$p->profile_id}}">
                        <div class="content">
                            {{$p->first_name}} {{$p->last_name}}
                        </div>
                    </a>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="row">
            <div class="sixteen wide column">
                <div class="ui compact menu" style="float:right">
                    <a href="/users/filesystem/list-documents/{{$profile_id}}|receipts" class="item {{($paId !== (int)$profile_id) ? 'disabled' : ''}} {{(!empty($folder) and $folder === 'receipts') ? 'active blue' : ''}}">
                        <i class="folder icon"></i>receipts
                    </a>
                    <a href="/users/filesystem/list-documents/{{$profile_id}}|payments" class="item {{($paId !== (int)$profile_id) ? 'disabled' : ''}} {{(!empty($folder) and $folder === 'payments') ? 'active blue' : ''}}">
                        <i class="folder icon"></i>payment requests
                    </a>
                    <a href="/users/filesystem/list-documents/{{$profile_id}}|uploads" class="item {{(!empty($folder) and $folder === 'uploads') ? 'active blue' : ''}}">
                        <i class="folder icon"></i>uploads
                    </a>
                    <a href="/users/filesystem/list-documents/{{$profile_id}}|downloads" class="item {{(!empty($folder) and $folder === 'downloads') ? 'active blue' : ''}}">
                        <i class="folder icon"></i>government forms
                    </a>
                    <a href="/users/filesystem/list-documents/{{$profile_id}}|notary" class="item {{(!empty($folder) and $folder === 'notary') ? 'active blue' : ''}}">
                        <i class="folder icon"></i>notary forms
                    </a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="sixteen wide column">
                <table class="ui single line table">
                    <thead>
                        <tr>
                            <th class="four wide">Applicant Name</th>
                            <th class="six wide">File Name</th>
                            <th class="two wide">File Type</th>
                            <th class="two wide">Created Date</th>
                            <th class="two wide"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($files) > 0)
                        @foreach($files as $file)
                        <tr class="item {{$file['key']}}">
                            <td>{{$file['applicant']}}</td>
                            <td><i class="red large file pdf outline icon"></i>{{$file['file']}}</td>
                            <td><b>{{$file['type']}}</b></td>
                            <td><i>{{$file['date']}}</i></td>
                            <td>
                                <div class="ui buttons">
                                  <button class="ui negative delete button" id="{{$file['key']}}" data-fileName="{{$file['file']}}" data-folder="{{$folder}}">Delete</button>
                                  <div class="or"></div>
                                  <a class="ui positive download button" href="/users/filesystem/download-profile-{{$folder}}/{{$profile_id}}-{{$file['file']}}">Download</a>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td>
                                <div class="heading">
                                    <h2>Folder Empty</h2>
                                </div>
                            </td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

    <input type="hidden" id="token" style="display: none" value="{{ csrf_token() }}" />
    <input type="hidden" id="profile_id" style="display: none" value="{{ $profile_id }}" />

@stop

@section('script')
    <script src="{{ URL::asset('js/file_manager.js') }}"></script>
    <script src="{{ URL::asset('js/documents.js') }}"></script>
@stop
