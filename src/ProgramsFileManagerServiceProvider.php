<?php

namespace hpsadev\ProgramsFileManager;

use Illuminate\Support\ServiceProvider;

class ProgramsFileManagerServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {

        if (!$this->app->routesAreCached()) {
            require __DIR__.'/routes.php';
        }

        $this->loadViewsFrom(__DIR__.'/Views', 'programsfilemanager');

        $this->publishes([
            __DIR__.'/programsfilemanager.php'  => config_path('programsfilemanager.php'),
            __DIR__.'/assets/file_upload.js'    => public_path().'/js/file_upload.js',
            __DIR__.'/assets/file_manager.js'   => public_path().'/js/file_manager.js',
            __DIR__.'/assets/documents.js'      => public_path().'/js/documents.js',
            __DIR__.'/Views'                    => base_path("resources/views/vendor/programsfilemanager"),
        ]);
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
