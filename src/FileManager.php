<?php

namespace hpsadev\ProgramsFileManager;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use hpsadev\ProgramsFileManager\DirectoryManager;
use App\Application;
use App\Profile;
use App\Address;
use App\FamilyMember;
use Auth;
use Input;
use Storage;

/**
 * HP Online Applications File Upload/Download/Find Management
 * Class.
 *
 * @author      Gershon Koks <gershon.koks@henleyglobal.com>
 * @copyright   Henley & Partners Ltd
 * @license     For Henley & Partners use only.
 * @version     1.1
 */
class FileManager {

    protected $profile_id;
    protected $user_id;
    protected $uploads_path;
    protected $notary_path;
    protected $payments_path;
    protected $receipts_path;
    protected $folders;

    /**
     * List Of User Uploads File Types
     * @var array
     */
    private $fileTypes = ['Passport', 'Residency-Card', 'National-ID', 'Birth-Certificate', 'Proof-of-Residential-Address', 'Marriage-Certificate', 'Divorse-Certificate', 'Letter-of-Application', 'Professional-Reference', 'Personal-Reference', 'Power-of-Attorney', 'Affidavit-of-Funds', 'Letter-of-Employment', 'Letter-of-Recommendation', 'Police-Certificate', 'Affidavit-from-Parent', 'Banks-Recommendations', 'Business-Plan', 'Payment-Request-Service-Fees', 'Payment-Request-Government-Fees', 'Service-Fees-Receipt', 'Government-Fees-Receipt', 'D1-Disclosure', 'Millitary-Service', 'D2-Fingerprint-&-Signature', 'D3-Medical-Questionnaire', 'D4-Investment-Agreement', 'Passport-Application', 'Form-12'];

    /**
     * Class Constructor initialising internal variables
     * @since  1.1 Function Init
     * @param integer $profileID class required Profile ID
     */
    public function __construct($profileID = null) {
        $this->user_id          = ($profileID) ? Profile::find($profileID)->user->user_id : Auth::id();
        $this->profile_id       = $profileID;
        $this->payments_path    = storage_path("/app/users/".$this->user_id."/payments/");
        $this->receipts_path    = storage_path("/app/users/".$this->user_id."/receipts/");
        $this->uploads_path     = storage_path("/app/users/".$this->user_id."/profiles/".$this->profile_id.'/uploads/');
        $this->downloads_path   = storage_path("/app/users/".$this->user_id."/profiles/".$this->profile_id.'/downloads/');
        $this->notary_path      = storage_path("/app/users/".$this->user_id."/profiles/".$this->profile_id.'/notary/');
        $this->folders          = [
            'uploads'   => $this->uploads_path,
            'downloads' => $this->downloads_path,
            'notary'    => $this->notary_path,
            'payments'  => $this->payments_path,
            'receipts'  => $this->receipts_path
        ];
    }

    /**
     * Class Public Method for uploading a PDF to
     * the Profile Owners Uploads Directory.
     *
     * @since   1.0   Function Init
     * @param   POST    $request    POST values
     * @return  string              file upload success indicator
     */
    public function toProfileUploads($request) {
        return $this->uploadFile($request, $this->uploads_path);
    }

    /**
     * Upper Level File Upload to Profile Notary path.
     *
     * @since  1.1 Function Init
     * @param  post     $request    Post Data
     * @return response             upload response
     */
    public function toProfileNotary($request) {
        return $this->uploadFile($request, $this->notary_path);
    }

    /**
     * Upper Level File Upload to Profile Paymentx path.
     *
     * @since  1.1 Function Init
     * @param  post     $request    Post Data
     * @return response             upload response
     */
    public function toProfilePayments($request) {
        return $this->uploadFile($request, $this->payments_path);
    }

    /**
     * Upper Level File Upload to Profile Receipts path.
     *
     * @since  1.1 Function Init
     * @param  post     $request    Post Data
     * @return response             upload response
     */
    public function toProfileReceipts($request) {
        return $this->uploadFile($request, $this->receipts_path);
    }

    /**
     * Upper Level File Download from Profile Downloads path.
     *
     * @since  1.1 Function Init
     * @param  post     $request    Post Data
     * @return response             download response
     */
    public function fromDownloads($fileName) {
        return $this->downloadFile($fileName, 'downloads');
    }

    /**
     * Upper Level File Download from Profile Uploads path.
     *
     * @since  1.1 Function Init
     * @param  post     $request    Post Data
     * @return response             download response
     */
    public function fromUploads($fileName) {
        return $this->downloadFile($fileName, 'uploads');
    }

    /**
     * Upper Level File Download from Profile Notary path.
     *
     * @since  1.1 Function Init
     * @param  post     $request    Post Data
     * @return response             download response
     */
    public function fromNotary($fileName) {
        return $this->downloadFile($fileName, 'notary');
    }

    /**
     * Upper Level File Download from Profile Payments path.
     *
     * @since  1.1 Function Init
     * @param  post     $request    Post Data
     * @return response             download response
     */
    public function fromPayments($fileName) {
        return $this->downloadFile($fileName, 'payments');
    }

    /**
     * Upper Level File Download from Profile Receipts path.
     *
     * @since  1.1 Function Init
     * @param  post     $request    Post Data
     * @return response             download response
     */
    public function fromReceipts($fileName) {
        return $this->downloadFile($fileName, 'receipts');
    }

    /**
     * Class Public Method for Deleting a PDF from
     * the Profile Owners Uploids Directory.
     *
     * @since   1.0   Function Init
     * @param   POST    $request    POST values
     * @return  string              file delete success indicator
     */
    public function delFromUploads($request) {
        $fullPath = $this->uploads_path.$request['file'];
        if (file_exists($fullPath)) {
            unlink($fullPath);
            return $request['id'];
        } else {
            return 'error';
        }
    }

    /**
     * Class Public Method for Deleting a PDF from
     * the Profile Owners Notary Directory.
     *
     * @since   1.0   Function Init
     * @param   POST    $request    POST values
     * @return  string              file delete success indicator
     */
    public function delFromNotary($request) {
        $fullPath = $this->notary_path.$request['file'];
        if (file_exists($fullPath)) {
            unlink($fullPath);
            return $request['id'];
        } else {
            return 'error';
        }
    }

    /**
     * Class Public Method for Deleting a PDF from
     * the Profile Owners Payments Directory.
     *
     * @since   1.1   Function Init
     * @param   POST    $request    POST values
     * @return  string              file delete success indicator
     */
    public function delFromPayments($request) {
        $fullPath = $this->payments_path.$request['file'];
        if (file_exists($fullPath)) {
            unlink($fullPath);
            return $request['id'];
        } else {
            return 'error';
        }
    }

    /**
     * Class Public Method for Deleting a PDF from
     * the Profile Owners Receipt Directory.
     *
     * @since   1.1   Function Init
     * @param   POST    $request    POST values
     * @return  string              file delete success indicator
     */
    public function delFromReceipts($request) {
        $fullPath = $this->receipts_path.$request['file'];
        if (file_exists($fullPath)) {
            unlink($fullPath);
            return $request['id'];
        } else {
            return 'error';
        }
    }

    /**
     * Class Public Method for Deleting a PDF from
     * the Profile Owners Downloads Directory.
     *
     * @since   1.1   Function Init
     * @param   POST    $request    POST values
     * @return  string              file delete success indicator
     */
    public function delFromDownloads($request) {
        $fullPath = $this->downloads_path.$request['file'];
        if (file_exists($fullPath)) {
            unlink($fullPath);
            return $request['id'];
        } else {
            return 'error';
        }
    }

    /**
     * Get all User Uploads.
     * @param  string $key filter = profileId|Directory
     * @return array       key=>value array of files
     */
    public function getAllUploads($key) {
        $keys       = explode('|', $key);
        $profile    = Profile::find($keys[0]);
        $fileArr    = [];
        $folder     = (count($keys) <= 1) ? 'uploads' : $keys[1];
        $counter    = 0;
        if ($folder === 'receipts' || $folder === 'payments') {
            $path   = storage_path("/app/users/".$this->user_id."/".$folder.'/');
        } else {
            $path   = storage_path("/app/users/".$this->user_id."/profiles/".$profile->profile_id.'/'.$folder.'/');
        }
        $files      = scandir($path);
        foreach ($files as $file) {
            if ($file === '.' || $file === '..') {
                continue;
            }
            $splFileName = explode('.', $file);
            array_push($fileArr, [
                'applicant' => $profile->first_name.' '.$profile->last_name,
                'file'      => $file,
                'type'      => str_replace('-', ' ', $this->lkpFileType($file)),
                'date'      => date("F d Y", filemtime($path.$file)),
                'key'       => strtolower($this->lkpFileType($file).'-'.explode('-', $splFileName[0])[count(explode('-', $splFileName[0])) - 1]),
            ]);
        }
        return $fileArr;
    }

    /**
     * Class public method which gets list of files as array
     * from Profile owners uploads Directory.
     *
     * @since   1.0   Function Init
     * @param   string  $documentTypes  Document Type that will be looked for.
     * @param   string  $directory      Folder to search and find file type.
     * @return  array                   compiled array for lookup up files.
     */
    public function getProfileFiles($documentTypes, $directory) {
        return $this->getFilesByType($documentTypes,$this->folders[$directory]);
    }

    /**
     * Get All files [Not in key=>value] specified array.
     *
     * @since   1.1   Function Init
     * @param  string   $directory  directory
     * @param  string   $types      File Type
     * @return array                array of files.
     */
    public function getAllProfileFiles($directory = NULL, $types = array()) {
        $holder = [];
        if ($directory) {
            if (is_array($directory)) {
                foreach ($directory as $dir) {
                    $folders[$dir] = $this->folders[$dir];
                }
            } else {
                $folders = [$this->folders[$directory]];
            }
        } else {
            $folders = $this->folders;
        }
        $fileTypes  = (!empty($types)) ? $types : $this->fileTypes;
        foreach ($folders as $folder) {
            foreach ($fileTypes as $type) {
                $key = strtolower(str_replace('-', '_', $type));
                if (!empty($return[$key])) {
                    continue;
                }
                $return[$key] = $this->getFilesByType($type, $folder);
            }

        }
        return $return;
    }

    /**
     * Internal helper function which lookup files
     * from Directory based on Document Type.
     *
     * @since   1.0   Function Init
     * @param  string   $fileType   Document Type that will be looked up.
     * @param  string   $path       Path from which files will be looked up from.
     * @return array                compiled array for lookup up files.
     */
    private function getFilesByType($docType, $path) {
        $counter        = 0;
        $fileArr        = [];
        $uploadFiles    = scandir($path);
        foreach ($uploadFiles as $i => $file) {
            if ($i === 0 || $i === 1) {
                continue;
            }

            $filExtSpl      = explode('.',$file);
            $fileNameSpl    = explode('-',$filExtSpl[0]);
            if (strpos($file, $docType) === false) {
                continue;
            }

            ++$counter;
            array_push($fileArr, [
                'key'   => strtolower($docType).'-'.$counter,
                'file'  => $file
            ]);
        }
        return $fileArr;
    }

    /**
     * Get File by Name
     *
     * @since   1.1   Function Init
     * @param  string   $docType    Document Type
     * @param  string   $path       File Path
     * @return string               Full File Path
     */
    public function getFileByName($docType, $path) {
        $counter        = 0;
        $fileArr        = [];
        $uploadFiles    = scandir($path);
        foreach ($uploadFiles as $i => $file) {
            if ($i === 0 || $i === 1) {
                continue;
            }

            $filExtSpl      = explode('.',$file);
            $fileNameSpl    = explode('-',$filExtSpl[0]);
            if (strpos($file, $docType) === false) {
                continue;
            } else {
                return $path.$file;
            }

        }
    }

    /**
     * Internal Class function for executing actual PDF
     * file upload.
     *
     * @since   1.0   Function Init
     * @param   POST    $request    POST values
     * @param   string  $path       upload path
     * @return  string              upload success indicator
     */
    private function uploadFile($request, $path) {
        $profile = Profile::find($request->profile_id);
        foreach ($request->all() as $key => $value) {
            if (Input::hasFile($key)) {
                $file   = Input::file($key);
                $ext    = $file->getClientOriginalExtension();
                if ($file->isValid() && $ext === 'pdf') {
                    $split  = explode('-', $key);
                    $name   = $this->changeFileName($key).'-'.$split[1].'.'.$ext;
                    $moved  = $file->move($path, $name);
                    $fullFileName   = $path.$name;
                    $pb             = new PdfBuilder('Encrypt', $profile);
                    $encypted       = $pb->encryptUpload($fullFileName);
                    if (!$moved && $encypted !== '1') {
                        return 'failed';
                    } else {
                        return $name;
                    }
                } else {
                    return 'failed';
                }
            } else {
                continue;
            }
        }
        return 'failed';
    }

    /**
     * Low Level Dowload File From Directory Functions.
     *
     * @since   1.0   Function Init
     * @param  string   $fileName   File Name
     * @param  string   $folder     Path to download from
     * @return Response             Download Response
     */
    private function downloadFile($fileName, $folder) {
        $split      = explode('-', $fileName);
        $profile    = Profile::find($split[0]);
        $user       = $profile->user;
        if($folder === 'receipts' || $folder === 'payments') {
            $baseDir = storage_path('app').'/users/'.$user->user_id.'/'.$folder.'/';
        } else {
            $baseDir = storage_path('app').'/users/'.$user->user_id.'/profiles/'.$split[0].'/'.$folder.'/';
        }
        $file = $baseDir.substr($fileName, (strlen($split[0]) + 1));
        if (Auth::user()->group->roles === 'admin') {
            $file       = $this->moveToTmp($fileName, $file);
            $pb         = new PdfBuilder('Decrypt', $profile);
            $encypted   = $pb->encryptForAdmin($file);
        }
        return response()->download($file, substr($fileName, (strlen($split[0]) + 1)), array('Content-Type: application/pdf'));
    }

    /**
     * Moves file to temporary filr location.
     *
     * @since   1.1   Function Init
     * @param  string   $fileName   File Name
     * @param  string   $origFile   Original File to be moved
     * @return string               Moved File
     */
    private function moveToTmp($fileName, $origFile) {
        $path       = storage_path('app/tmp/');
        $fullPath   = $path.$fileName;
        shell_exec('rm -r -f '.$path.'*');
        if (copy($origFile, $fullPath)) {
            return $fullPath;
        } else {
            return NULL;
        }

    }

    /**
     * Internal class method for renaming uploaded file name.
     *
     * @since   1.0   Function Init
     * @param   string  $fileName   Input file name
     * @return  string              new file name
     */
    private function changeFileName($fileName) {
        $profile = Profile::find($this->profile_id);
        if (strpos($fileName, 'passport') !== false) {
            return $profile->first_name.'-'.$profile->last_name.'-Passport';
        } else if (strpos($fileName, 'residency') !== false) {
            return $profile->first_name.'-'.$profile->last_name.'-Residency-Card';
        } else if (strpos($fileName, 'national_id') !== false) {
            return $profile->first_name.'-'.$profile->last_name.'-National-ID';
        } else if (strpos($fileName, 'birth_certificate') !== false) {
            return $profile->first_name.'-'.$profile->last_name.'-Birth-Certificate';
        } else if (strpos($fileName, 'proof_of_residential_address') !== false) {
            return $profile->first_name.'-'.$profile->last_name.'-Proof-of-Residential-Address';
        } else if (strpos($fileName, 'marriage_certificate') !== false) {
            return $profile->first_name.'-'.$profile->last_name.'-Marriage-Certificate';
        } else if (strpos($fileName, 'divorse_document') !== false) {
            return $profile->first_name.'-'.$profile->last_name.'-Divorse-Certificate';
        } else if (strpos($fileName, 'letter_of_application') !== false) {
            return $profile->first_name.'-'.$profile->last_name.'-Letter-of-Application';
        } else if (strpos($fileName, 'professional_reference') !== false) {
            return $profile->first_name.'-'.$profile->last_name.'-Professional-Reference';
        } else if (strpos($fileName, 'personal_reference') !== false) {
            return $profile->first_name.'-'.$profile->last_name.'-Personal-Reference';
        } else if (strpos($fileName, 'power_of_attorney') !== false) {
            return $profile->first_name.'-'.$profile->last_name.'-Power-of-Attorney';
        } else if (strpos($fileName, 'affidavit_funds') !== false) {
            return $profile->first_name.'-'.$profile->last_name.'-Affidavit-of-Funds';
        } else if (strpos($fileName, 'letter_of_employment') !== false) {
            return $profile->first_name.'-'.$profile->last_name.'-Letter-of-Employment';
        } else if (strpos($fileName, 'letter_of_recommendation') !== false) {
            return $profile->first_name.'-'.$profile->last_name.'-Letter-of-Recommendation';
        } else if (strpos($fileName, 'police_certificate') !== false) {
            return $profile->first_name.'-'.$profile->last_name.'-Police-Certificate';
        } else if (strpos($fileName, 'affidavit_from_parent') !== false) {
            return $profile->first_name.'-'.$profile->last_name.'-Affidavit-from-Parent';
        } else if (strpos($fileName, 'banks_recommendations') !== false) {
            return $profile->first_name.'-'.$profile->last_name.'-Banks-Recommendations';
        } else if (strpos($fileName, 'business_plan') !== false) {
            return $profile->first_name.'-'.$profile->last_name.'-Business-Plan';
        } else if (strpos($fileName, 'request_service') !== false) {
            return $profile->first_name.'-'.$profile->last_name.'-Payment-Request-Service-Fees';
        } else if (strpos($fileName, 'request_gov') !== false) {
            return $profile->first_name.'-'.$profile->last_name.'-Payment-Request-Government-Fees';
        } else if (strpos($fileName, 'receipt_service') !== false) {
            return $profile->first_name.'-'.$profile->last_name.'-Service-Fees-Receipt';
        } else if (strpos($fileName, 'receipt_gov') !== false) {
            return $profile->first_name.'-'.$profile->last_name.'-Government-Fees-Receipt';
        } else if (strpos($fileName, 'millitary_service') !== false) {
            return $profile->first_name.'-'.$profile->last_name.'-Millitary-Service';
        } else if (strpos($fileName, 'D1') !== false) {
            return $profile->first_name.'-'.$profile->last_name.'-D1-Disclosure';
        } else if (strpos($fileName, 'D2') !== false) {
            return $profile->first_name.'-'.$profile->last_name.'-D2-Fingerprint-&-Signature';
        } else if (strpos($fileName, 'D3') !== false) {
            return $profile->first_name.'-'.$profile->last_name.'-D3-Medical-Questionnaire';
        } else if (strpos($fileName, 'D4') !== false) {
            return $profile->first_name.'-'.$profile->last_name.'-D4-Investment-Agreement';
        } else if (strpos($fileName, 'Passport') !== false) {
            return $profile->first_name.'-'.$profile->last_name.'-DOM-Passport-Application';
        } else if (strpos($fileName, 'F12') !== false) {
            return $profile->first_name.'-'.$profile->last_name.'-Form-12';
        }
    }

    /**
     * Lookup file type by File Name.
     *
     * @since   1.1   Function Init
     * @param  string $file File Name
     * @return string       File Type
     */
    private function lkpFileType($file) {
        if (strpos($file, 'Passport') !== false) {
            return 'Passport';
        } else if (strpos($file, 'Residency-Card') !== false) {
            return 'Residency-Card';
        } else if (strpos($file, 'National-ID') !== false) {
            return 'National-ID';
        } else if (strpos($file, 'Birth-Certificate') !== false) {
            return 'Birth-Certificate';
        } else if (strpos($file, 'Proof-of-Residential-Address') !== false) {
            return 'Proof-of-Residential-Address';
        } else if (strpos($file, 'Marriage-Certificate') !== false) {
            return 'Marriage-Certificate';
        } else if (strpos($file, 'Divorse-Certificate') !== false) {
            return 'Divorse-Certificate';
        } else if (strpos($file, 'Letter-of-Application') !== false) {
            return 'Letter-of-Application';
        } else if (strpos($file, 'Professional-Reference') !== false) {
            return 'Professional-Reference';
        } else if (strpos($file, 'Personal-Reference') !== false) {
            return 'Personal-Reference';
        } else if (strpos($file, 'Power-of-Attorney') !== false) {
            return 'Power-of-Attorney';
        } else if (strpos($file, 'Affidavit-of-Funds') !== false) {
            return 'Affidavit-of-Funds';
        } else if (strpos($file, 'Letter-of-Employment') !== false) {
            return 'Letter-of-Employment';
        } else if (strpos($file, 'Letter-of-Recommendation') !== false) {
            return 'Letter-of-Recommendation';
        } else if (strpos($file, 'Police-Certificate') !== false) {
            return 'Police-Certificate';
        } else if (strpos($file, 'Affidavit-from-Parent') !== false) {
            return 'Affidavit-from-Parent';
        } else if (strpos($file, 'Banks-Recommendations') !== false) {
            return 'Banks-Recommendations';
        } else if (strpos($file, 'Business-Plan') !== false) {
            return 'Business-Plan';
        } else if (strpos($file, 'Millitary-Service') !== false) {
            return 'Millitary-Service';
        } else if (strpos($file, 'D1') !== false) {
            return 'D1-Disclosure';
        } else if (strpos($file, 'Payment-Request-Service-Fees') !== false) {
            return 'Payment-Request-Service-Fees';
        } else if (strpos($file, 'Payment-Request-Government-Fees') !== false) {
            return 'Payment-Request-Government-Fees';
        } else if (strpos($file, 'Service-Fees-Receipt') !== false) {
            return 'Service-Fees-Receipt';
        } else if (strpos($file, 'Government-Fees-Receipt') !== false) {
            return 'Government-Fees-Receipt';
        } else if (strpos($file, 'D2') !== false) {
            return 'D2-Fingerprint-&-Signature';
        } else if (strpos($file, 'D3') !== false) {
            return 'D3-Medical-Questionnaire';
        } else if (strpos($file, 'D4') !== false) {
            return 'D4-Investment-Agreement';
        } else if (strpos($file, 'Passport') !== false) {
            return 'Passport-Application';
        } else if (strpos($file, 'Form-12') !== false) {
            return 'Form-12';
        }
    }

}
