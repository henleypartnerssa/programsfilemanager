<?php

namespace hpsadev\ProgramsFileManager\Mappers;

use Carbon\Carbon;

/**
 * Dominica Passport Mapper
 *
 * @author      Gershon Koks <gershon.koks@henleyglobal.com>
 * @copyright   Henley & Partners Ltd
 * @license     For Henley & Partners use only.
 * @version     1.1
 */
class PassportMapper extends Mapper
{

    function __construct($profile)
    {
        $this->profile  = $profile;
        $this->user     = $this->profile->user;
        $this->app      = $this->profile->application;
        $this->mapper();
        $this->minors();
        $this->nameChange();
    }

    protected function mapper() {
        $dob = Carbon::parse($this->profile->date_of_birth);
        $dom = Carbon::parse($this->profile->date_of_marriage);
        $this->fields['surname']                    = $this->profile->last_name;
        $this->fields['maiden name']                = $this->profile->maiden_name;
        $this->fields['first name']                 = $this->profile->first_name;
        $this->fields['second name']                = (!empty($this->profile->middle_names) && !empty(explode(' ', $this->profile->middle_names)[0])) ? explode(' ', $this->profile->middle_names)[0] : '';
        $this->fields['third name']                 = (!empty($this->profile->middle_names) && !empty(explode(' ', $this->profile->middle_names)[1])) ? explode(' ', $this->profile->middle_names)[1] : '';
        $this->fields['title and surname']          = $this->profile->title.' '.$this->profile->last_name;
        $this->fields['christian name']             = $this->profile->first_name.' '.$this->profile->middle_names;
        $this->fields['age']                        = (string)$this->getAge();
        $this->fields['place of birth']             = $this->profile->place_of_birth;
        $this->fields['dob day']                    = (string)$dob->day;
        $this->fields['dob month']                  = (string)$dob->month;
        $this->fields['dob year']                   = (string)$dob->year;
        $this->fields['occupation']                 = (!empty($this->getBusiness()->primary_occupation)) ? $this->getBusiness()->primary_occupation : 'N/A';
        $this->fields['address']                    = $this->addressStr();
        $this->fields['residence']                  = $this->addressStr();
        $this->fields['phone']                      = $this->profile->personal_landline_no;
        $this->fields['fax']                        = ($this->profile->personal_fax_no) ? $this->profile->personal_fax_no : '';
        $this->fields['email']                      = $this->profile->personal_email_address;
        if(!empty($this->profile->marital_status)) {
            $this->fields[$this->profile->marital_status] = $this->profile->marital_status;
        }
        $this->fields['height feet']                = (string)($this->profile->height * 0.0328084);
        $this->fields['height inches']              = (string)($this->profile->height * 0.393701);
        $this->fields['hair color']                 = $this->profile->hair_color;
        $this->fields['eye color']                  = $this->profile->eye_color;
        $this->fields['marks']                      = $this->profile->distinguising_marks;
        $this->fields['husband surname']            = ($this->profile->gender === 'female') ? $this->getSpouse()->last_name : '';
        $this->fields['husband christian names']    = ($this->profile->gender === 'female') ? $this->getSpouse()->first_name.' '.$this->getSpouse()->middle_names : '';
        $this->fields['place of marriage']          = ($this->profile->gender === 'female') ? (!empty($this->profile->place_of_marriage)) ? $this->profile->place_of_marriage : '' : '';
        $this->fields['dom_day']                    = ($this->profile->gender === 'female') ? (string)$dom->day : '';
        $this->fields['dom_month']                  = ($this->profile->gender === 'female') ? (string)$dom->month : '';
        $this->fields['dom_year']                   = ($this->profile->gender === 'female') ? (string)$dom->year : '';
    }

    protected function nameChange() {
        if ($this->profile->changed_name) {
            $this->fields['name change yes']        = 'name change yes';
            $this->fields['name change message']    = $this->profile->name_change_reason;
        } else {
            $this->fields['name change no']        = 'name change no';
        }
    }

    protected function minors() {
        $profiles   = $this->user->profiles()->get();
        $counter    = 0;
        foreach ($profiles as $p) {
            $now = Carbon::now();
            $age = Carbon::parse($p->date_of_birth)->diff($now)->y;
            if ($age < 16) {
                ++$counter;
                $this->fields['child name '.$counter]           = $p->first_name;
                $this->fields['child surname '.$counter]        = $p->last_name;
                $this->fields['child pob '.$counter]            = $p->place_of_birth;
                $this->fields['child dob '.$counter]            = $p->date_of_birth;
                $this->fields['child gender '.$counter]         = $p->gender;
                $this->fields['child relationship '.$counter]   = $p->belongs_to;
            }
        }
    }

}
