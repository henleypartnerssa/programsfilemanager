<?php

namespace hpsadev\ProgramsFileManager\Mappers;

use App\Address;
use Carbon\Carbon;
/**
 * Abstract Class Mapper
 *
 * @author      Gershon Koks <gershon.koks@henleyglobal.com>
 * @copyright   Henley & Partners Ltd
 * @license     For Henley & Partners use only.
 * @version     1.2
 */
abstract class Mapper {

    /**
     * Given Profile that should be instiated at all time.
     * @var Profile
     */
    protected $profile;
    /**
     * Given User that should be instiated at all time.
     * @var User
     */
    protected $user;
    /**
     * Given Application that should be instiated at all time.
     * @var Application
     */
    protected $app;
    /**
     * Array of mapped fields. (Sould not be instiated but built via mapper function)
     * @var [type]
     */
    protected $fields;

    /**
     * Each child class inheriting should declare this
     * method to mapper fields for PDF Form.
     *  @since 1.1   Function Init
     */
    protected abstract function mapper();

    /**
     * Global Method used to retrieve mapped fields.
     * @since 1.1   Function Init
     * @return Array Mapped Fields
     */
    public function getFields() {
        return $this->fields;
    }

    /**
     * Global Method used to assist when mapping
     * multiline fields within the PDF Doc.
     *
     * @since 1.1   Function Init
     * @param  string   $field          Field Name or Indicator
     * @param  string   $text           String value to be mapped to field.
     * @param  integer  $maxChar        max number of characters to be mapped per line.
     * @param  string   $maxSentances   Max lines to be mapped to.
     * @return Array                    mapped field
     */
    protected function multiline($field, $text, $maxChar, $maxSentances) {
        $char_coount    = $maxChar;
        $sentance_count = 1;
        $max_sentance   = $maxSentances;
        $split          = explode(' ', $text);
        $array          = [];
        $sentance       = "";
        foreach ($split as $key => $value) {
            $tmp = $sentance.' '.$value;
            if (strlen($tmp) > $char_coount) {
                if($sentance_count !== $max_sentance) {
                    $array[$field.'_'.$sentance_count] = $sentance;
                    $sentance = $value;
                    ++$sentance_count;
                } else if ($sentance_count > $max_sentance) {
                    return $array;
                }
            } else {
                $sentance = $tmp;
            }
            if ($key === (count($split) - 1)) {
                $array[$field.'_'.$sentance_count] = $sentance;
            }
        }
        return $array;
    }

    /**
     * Normalise rext input data.
     * @since 1.2 Function Init.
     * @param  atring $text textarea Value
     * @return string       normalised string
     */
    protected function normaliseTextAreaValue($text) {
        $raw    = str_replace("\r\n", "\\n", $text);
        $tst    = str_replace("\\n\\n", "\\n", $raw);
        return $tst;
    }

    /**
     * Build Cur Address string.
     *
     * @since 1.1   Function Init
     * @return string Profile Current Address
     */
    protected function addressStr() {
        $address = $this->profile->addresses()->where('address_type', 'cur')->first();
        return $address->street1.' '.$address->street2.', '.$address->country.', '.$address->town.', '.$address->postal_code;
    }

    /**
     * Get Profile Address
     *
     * @since 1.1   Function Init
     * @return Address Profile Address
     */
    protected function address() {
        return $this->profile->addresses()->where('address_type', 'cur')->first();
    }

    /**
     * Get instantiated Profile Passport
     *
     * @since 1.1   Function Init
     * @return Passport Profile Passport.
     */
    protected function getPassport() {
        return $this->profile->identity_documents()->where('document_type', 'passport')->first();
    }

    /**
     * Get Specified Profile Passport.
     *
     * @since 1.1   Function Init
     * @param  Profile $profile Profile
     * @return Passport          Profile Passport
     */
    protected function getProfilePassport($profile) {
        return $profile->identity_documents()->where('document_type', 'passport')->first();
    }

    /**
     * Get isntantiated Profile Business Object
     * @since 1.1   Function Init
     * @return Business Profile Business
     */
    protected function getBusiness() {
        return $this->profile->Work_or_businesses()->first();
    }

    /**
     * Get instiated Profile medical object.
     * @since 1.1   Function Init
     * @return Medical Profile Medical Object
     */
    protected function getMedical() {
        return $this->profile->medical;
    }

    /**
     * Get instiated Profile address as string.
     * @since 1.1   Function Init
     * @return string address.
     */
    protected function getMedicalAddrStr() {
        $medical    = $this->profile->medical;
        $address    = Address::find($medical->address_id);
        return $address->street1.' '.$address->street2.', '.$address->country.', '.$address->town.', '.$address->postal_code;
    }

    /**
     * Get instiated Profile age
     * @since 1.1   Function Init
     * @return integer age
     */
    protected function getAge() {
        $now    = Carbon::now();
        $dob    = Carbon::parse($this->profile->date_of_birth);
        return $dob->diff($now)->y;
    }

    /**
     * Get instiated Profile Spouse.
     * @since 1.1   Function Init
     * @return mixed Profile or FamilyMember Object
     */
    protected function getSpouse() {
        $spouse = NULL;
        if ($this->profile->belongs_to === 'Spouse') {
            $spouse = $this->user->profiles()->where('belongs_to', 'Principal Applicant')->first();
        } else {
            $spouse = $this->user->profiles()->where('belongs_to', 'Principal Applicant')->first();
        }
        return $spouse;
    }

}
