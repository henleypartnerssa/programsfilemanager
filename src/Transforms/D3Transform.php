<?php

namespace hpsadev\ProgramsFileManager\Transforms;

/**
 * Dominica D3 PDF form mapper transform methods.
 */
trait D3Transform
{
    protected function a_one() {
        $val = $this->getMedical()->a_one;
        if ($val !== 'no') {
            $spl = explode('|', $val);
            $this->fields['a_one '.trim($spl[0])]  = 'a_one '.trim($spl[0]);
            $this->fields['a_one message']      = trim($spl[1]);
        } else {
            $this->fields['a_one '.$val]        = 'a_one '.$val;
        }
    }

    protected function a_two() {
        $val = $this->getMedical()->a_two;
        if ($val !== 'no') {
            $spl = explode('|', $val);
            $this->fields['a_two '.trim($spl[0])]  = 'a_two '.trim($spl[0]);
            $this->fields['a_two message']      = trim($spl[1]);
        } else {
            $this->fields['a_two '.$val]        = 'a_two '.$val;
        }
    }

    protected function a_three() {
        $val = $this->getMedical()->a_three;
        if ($val !== 'no') {
            $spl = explode('|', $val);
            $this->fields['a_three '.trim($spl[0])]  = 'a_three '.trim($spl[0]);
            $this->fields['a_three message']      = trim($spl[1]);
        } else {
            $this->fields['a_three '.$val]        = 'a_three '.$val;
        }
    }

    protected function a_four() {
        $arr    = ['4a', '4b', '4c', '4d', '4e', '4f', '4g', '4h', '4i', '4j', '4k', '4l', '4m', '4n', '4o', '4p', '4q', '4r', '4s', '4t', '4u'];
        $val    = $this->getMedical()->a_four;
        $spl    = explode('|', $val);
        $items  = explode(',', trim($spl[0]));
        foreach ($arr as $i) {
            if (in_array($i, $items)) {
                $this->fields[$i.' yes'] = $i.' yes';
            } else {
                $this->fields[$i.' no'] = $i.' no';
            }
        }
        $this->fields['4 message'] = (!empty($spl[1])) ? $spl[1] : '';
    }
}
