<?php

namespace hpsadev\ProgramsFileManager\Mappers;

/**
 * Dominica D2 Mapper
 *
 * @author      Gershon Koks <gershon.koks@henleyglobal.com>
 * @copyright   Henley & Partners Ltd
 * @license     For Henley & Partners use only.
 * @version     1.1
 */
class D2Mapper extends Mapper
{

    function __construct($profile)
    {
        $this->profile  = $profile;
        $this->user     = $this->profile->user;
        $this->app      = $this->profile->application;
        $this->mapper();
    }

    protected function mapper() {
        $this->fields['Surname']                = $this->profile->last_name;
        $this->fields['Names']                  = $this->profile->first_name.' '.$this->profile->middle_names;
        $this->fields['Date of birth']          = $this->profile->date_of_birth;
        $this->fields['Place Country of Birth'] = $this->profile->place_of_birth.' '.$this->profile->country_of_birth;
        $this->fields['Address']                = $this->addressStr();
        $this->fields['Passport No']            = $this->profile->identity_documents()->where('document_type', 'passport')->first()->document_number;
        $this->fields['Passport Country']       = $this->profile->identity_documents()->where('document_type', 'passport')->first()->issuing_country;
        $this->fields[$this->profile->gender]   = $this->profile->gender;
    }

}
