<?php

namespace hpsadev\ProgramsFileManager\Transforms;

use App\LanguageType;
use Carbon\Carbon;
use App\Address;

/**
 * Dominica D1 PDF form mapper transform methods.
 */
trait D1Transform
{
    protected function partA() {
        $dob = Carbon::parse($this->profile->date_of_birth);
        $this->fields['first name']             = $this->profile->first_name;
        $this->fields['middle names']           = ($this->profile->middle_names) ? $this->profile->middle_names : 'N/A';
        $this->fields['other names']            = ($this->profile->changed_name) ? 'name changed to: '.$this->profile->changed_name.' on '.$this->profile->name_change_date.' for eason: '.$this->profile->name_change_reason : 'N/A';
        $this->fields['dob day']                = (string)$dob->day;
        $this->fields['dob month']              = (string)$dob->month;
        $this->fields['dob year']               = (string)$dob->year;
        $this->fields[$this->profile->gender]   = $this->profile->gender;
        $this->fields['place and country']      = $this->profile->place_of_birth.', '.$this->profile->country_of_birth;
        $this->fields['citizenship']            = $this->profile->citizenship_at_birth;
        $this->sepPassports();
        $this->fields['native name']            = ($this->profile->native_name) ? $this->profile->native_name : 'N/A';
        $this->fields['mothers maiden name']    = ($this->profile->mothers_maiden_name) ? $this->profile->mothers_maiden_name : 'N/A';
        $this->setCard('green_card');
        $this->setCard('social_security');
        $this->setCard('drivers_licence', true);

        if($this->profile->other_citizenship !== '') {
            $this->fields['other citizenship yes'] = 'other citizenship yes';
            $this->multiline("other citizenship line", $this->profile->other_citizenship, 89, 2);
        } else {
            $this->fields['other citizenship no'] = 'other citizenship no';
        }
        $this->fields['languages']              = $this->decodeLang($this->profile->languages);
        $this->includedMembers();
        if ($this->address()->street2) {
            $this->fields['current address'] = $this->address()->street1.', '.$this->address()->street2;
        } else {
            $this->fields['current address'] = $this->address()->street1;
        }
        $this->fields['current address city']               = $this->address()->town;
        $this->fields['current address country']            = $this->address()->country;
        $this->fields['current address zip code']           = $this->address()->postal_code;
        $this->fields['current address date since month']   = ($this->address()->date_since) ? explode('/', $this->address()->date_since)[0] : '';
        $this->fields['current address date since year']    = ($this->address()->date_since) ? explode('/', $this->address()->date_since)[1] : '';
        $this->postalAddr();
        $this->fields['home tel']       = $this->profile->personal_landline_no;
        $this->fields['mobile phone']   = $this->profile->personal_mobile_no;
        $this->fields['fax no']         = ($this->profile->personal_fax_no) ? $this->profile->personal_fax_no : 'N/A';
        $this->fields['email']          = $this->profile->personal_email_address;
        $this->fields['color of eye']   = $this->profile->eye_color;
        $this->fields['color of hair']  = $this->profile->hair_color;
        $this->fields['weight']         = $this->profile->weight;
        $this->fields['height']         = $this->profile->height;
        $this->fields['distinguishing'] = ($this->profile->distinguising_marks) ? $this->profile->distinguising_marks : 'N/A';
        $this->military();
        if ($this->profile->criminal_offences) {
            $this->fields['arrest record yes'] = 'arrest record yes';
            $this->multiline("arrest record", $this->profile->other_citizenship, 45, 4);
        } else {
            $this->fields['arrest record no'] = 'arrest record no';
        }
        $this->otherAddr();

    }

    protected function partB() {
        $business = $this->profile->Work_or_businesses()->first();
        $this->fields['occupation by training'] = ($business && $business->occupation_by_training) ? $business->occupation_by_training : 'N/A';
        $this->fields['primary occupation']     = ($business && $business->primary_occupation) ? $business->primary_occupation : 'N/A';
        if ($business && $business->self_employed) {
            $this->fields['self employed yes'] = 'self employed yes';
        } else {
            $this->fields['self employed no'] = 'self employed no';
        }
        $this->fields['primary business']       = ($business) ? $business->name_of_business : 'N/A';
        $this->fields['Nature of business']     = ($business) ? $this->normaliseTextAreaValue($business->nature_of_business) : 'N/A';
        $this->fields['business address']       = ($business) ? $this->businessAddr($business) : 'N/A';
        $this->fields['business tel']           = ($business) ? $business->phone_no : 'N/A';
        $this->fields['business fax']           = ($business) ? $business->fax_no : 'N/A';
        $this->fields['website']                = ($business) ? $business->web_url : 'N/A';
        $this->fields['registration number']    = ($business) ? $business->registration_no : 'N/A';
        $this->fields['list all companies']     = ($business) ? $this->normaliseTextAreaValue($business->companies_share_dir) : 'N/A';
        $this->fields['gross']                  = ($business && $business->gross_net_income) ? $business->gross_net_income : "N/A";
        $this->fields['net worth']              = ($business && $business->total_net_worth) ? $business->total_net_worth : 'N/A';
        $this->fields['business activities']    = ($business) ? $business->source_of_income : 'N/A';
        if ($business){
            $this->licenses($business);
        }
        if ($business && $business->disiplinary) {
            $this->fields['disiplinary yes']        = 'disiplinary yes';
            $this->fields['disiplinary message']    = $business->disiplinary;
        } else {
            $this->fields['disiplinary no']     = 'disiplinary no';
        }
        $this->fields['geo'] = ($business) ? $business->activities_geo : 'N/A';
        $this->fields['companies  persons'] = ($business) ? $business->mi_persons_companies : 'N/A';
        if ($business && $business->total_net_worth_summary) {
            $decoded = str_replace("\r\n", "", $business->total_net_worth_summary);
            $lines = $this->multiline("summary line", $decoded, 65, 5);
            foreach ($lines as $key => $line) {
                $this->fields[$key] = $line;
            }
        }
        $this->assetsLibs();
        $bank       = $this->profile->bank;
        if ($bank) {
            $bankAddr   = $bank->address;
            $this->fields['account name']       = $bank->account_name_of;
            $this->fields['account no']         = $bank->account_no;
            $this->fields['iban']               = $bank->account_iban_bic;
            $this->fields['bank and address']   = $bank->bank_name.' - '.$bankAddr->street1.' '.$bankAddr->street2.$bankAddr->country.$bankAddr->town.$bankAddr->postal_code;
        }
        $this->schools();
        $this->referenes();
    }

    protected function partC() {
        $dom = Carbon::parse($this->profile->date_of_marriage);
        if(!empty($this->profile->marital_status)) {
            $this->fields[$this->profile->marital_status] = $this->profile->marital_status;
        }
        $this->fields['date of marriage day']   = (string)$dom->day;
        $this->fields['date of marriage month'] = (string)$dom->month;
        $this->fields['date of marriage year']  = (string)$dom->year;
        $this->fields['place of marriage']      = ($this->profile->place_of_marriage) ? $this->profile->place_of_marriage : 'N/A';
        $this->spouse();
        $this->exSpouse();
        $this->familyMember('Father', 'father');
        $this->familyMember('Mother', 'mother');
        $this->familyMember('Father in-law', 'father inlaw', true);
        $this->familyMember('Mother in-law', 'mother inlaw', true);
        $this->siblings();
        $this->children();
    }

    protected function partD() {
        $decl   = $this->profile->declarations()->get();
        $msg    = '';
        foreach ($decl as $d) {
            if ($d->answer === true) {
                $this->fields[$d->code.' yes'] = $d->code.' yes';
                $msg = $msg.' '.$d->message;
            } else {
                $this->fields[$d->code.' no'] = $d->code.' no';
            }
        }
        $this->multiline("d_line", $msg, 76, 4);

        $char_ref = $this->profile->character_references()->get();
        $counter = 0;
        foreach ($char_ref as $c) {
            ++$counter;
            $addr = $c->address;
            $this->fields['ref fullname '.$counter]         = $c->full_name;
            $this->fields['ref address '.$counter]          = $addr->street1.' '.$addr->street2;
            $this->fields['ref city '.$counter]             = $addr->town;
            $this->fields['ref country and code '.$counter] = $addr->country.' '.$addr->postal_code;
            $this->fields['ref home phone '.$counter]       = $c->landline_no;
            $this->fields['ref mobile '.$counter]           = $c->mobile_no;
            $this->fields['ref email '.$counter]            = $c->email;
            $this->fields['ref years known '.$counter]      = (string)$c->years_known;
            $this->fields['ref occupation '.$counter]       = $c->occupation;
            $this->fields['ref employer '.$counter]         = $c->employer;
            $this->fields['ref work phone '.$counter]       = $c->work_no;
        }

    }

    protected function children() {
        if ($this->profile->belongs_to === 'Child') {
            return '';
        }
        $profiles   = $this->user->profiles()->where('belongs_to', 'Child')->get();
        $counter    = 0;
        if ($profiles->count() > 0) {
            foreach ($profiles as $p) {
                $address = $p->addresses()->where('address_type', 'cur')->first();
                $dob = Carbon::parse($p->date_of_birth);
                ++$counter;
                $this->fields['child surname '.$counter]        = $p->last_name;
                $this->fields['child name '.$counter]           = $p->first_name;
                $this->fields['child '.$p->gender.' '.$counter] = 'child '.$p->gender.' '.$counter;
                $this->fields['child dob day '.$counter]        = (string)$dob->day;
                $this->fields['child dob month '.$counter]      = (string)$dob->month;
                $this->fields['child dob year '.$counter]       = (string)$dob->year;
                $this->fields['child place of birth '.$counter] = $p->place_of_birth;
                $this->fields['child citizenship '.$counter]    = $p->citizenship_at_birth;
                $this->fields['child residence '.$counter]      = ($address) ? $address->street1.' '.$address->street2.', '.$address->town.', '.$address->country.', '.$address->postal_code : 'N/A';
                $this->fields['child occupation '.$counter]     = ($p->Work_or_businesses()->first()) ? $p->Work_or_businesses()->first()->primary_occupation : 'N/A';
                $this->fields['child included yes '.$counter]   = 'child included yes '.$counter;
            }
        } else {
            $children   = $this->profile->family_members()->where('relationship', 'Child')->get();
            foreach ($children as $p) {
                $address    = $p->addresses()->where('address_type', 'cur')->first();
                ++$counter;
                $this->fields['child surname '.$counter]        = $p->last_name;
                $this->fields['child name '.$counter]           = $p->first_name;
                $this->fields['child '.$p->gender.' '.$counter] = 'child '.$p->gender.' '.$counter;
                $this->fields['child dob day '.$counter]        = (string)$dob->day;
                $this->fields['child dob month '.$counter]      = (string)$dob->month;
                $this->fields['child dob year '.$counter]       = (string)$dob->year;
                $this->fields['child place of birth '.$counter] = $p->place_of_birth;
                $this->fields['child citizenship '.$counter]    = $p->citizenship;
                $this->fields['child residence '.$counter]      = ($address) ? $address->street1.' '.$address->street2.', '.$address->town.', '.$address->country.', '.$address->postal_code : 'N/A';
                $this->fields['child occupation '.$counter]     = $p->occupation;
                $this->fields['child included no '.$counter]    = $field.' included no';
            }
        }
    }

    protected function siblings() {
        // HACK: For some reason orWhere grouping does not work!!! thus having to use merge function
        $brothers   = $this->profile->family_members()->where('relationship', 'Brother')->get();
        $sisters    = $this->profile->family_members()->where('relationship', 'Sister')->get();
        $sibs       = $brothers->merge($sisters);
        $counter    = 0;
        foreach ($sibs as $s) {
            $address = $s->addresses()->where('address_type', 'cur')->first();
            ++$counter;
            $dob = Carbon::parse($s->date_of_birth);
            $this->fields['fam surname '.$counter]        = $s->last_name;
            $this->fields['fam name '.$counter]           = $s->first_name;
            $this->fields['fam dob day '.$counter]        = (string)$dob->day;
            $this->fields['fam dob month '.$counter]      = (string)$dob->month;
            $this->fields['fam dob year '.$counter]       = (string)$dob->year;
            $this->fields['fam place of birth '.$counter] = $s->place_of_birth;
            $this->fields['fam citizenship '.$counter]    = $s->citizenship;
            $this->fields['fam address '.$counter]        = ($address) ? $address->street1.' '.$address->street2.', '.$address->town.', '.$address->country.', '.$address->postal_code : 'N/A';
            $this->fields['fam occupation '.$counter]     = $s->occupation;
            $this->fields['fam '.$s->gender.' '.$counter] = 'fam '.$s->gender.' '.$counter;
        }
    }

    protected function familyMember($type, $field, $multi = false) {
        $hasProfile = $this->user->profiles()->where('belongs_to', $type)->count();
        if ($hasProfile) {
            $fm         = $this->user->profiles()->where('belongs_to', $type)->first();
            $address    = $fm->addresses()->where('address_type', 'cur')->first();
            $dob = Carbon::parse($fm->date_of_birth);
            $this->fields[$field.' surname']        = $fm->last_name;
            $this->fields[$field.' name']           = $fm->first_name;
            $this->fields[$field.' dob day']        = (string)$dob->day;
            $this->fields[$field.' dob month']      = (string)$dob->month;
            $this->fields[$field.' dob year']       = (string)$dob->year;
            $this->fields[$field.' place of birth'] = $fm->place_of_birth;
            $this->fields[$field.' citizenship']    = ($hasProfile) ? $fm->citizenship_at_birth : $fm->citizenship;
            $this->fields[$field.' address']        = $address->street1.' '.$address->street2.', '.$address->town.', '.$address->country.', '.$address->postal_code;
            $this->fields[$field.' occupation']     = ($hasProfile) ? $fm->Work_or_businesses()->first()->primary_occupation : $fm->occupation;
            if ($hasProfile) {
                $this->fields[$field.' included yes'] = $field.' included yes';
            } else {
                $this->fields[$field.' included no'] = $field.' included no';
            }
        } else {
            $fm         = $this->profile->family_members()->where('relationship', $type)->get();
            $counter    = 0;
            foreach($fm as $f) {
                $dob = Carbon::parse($f->date_of_birth);
                $address = $f->addresses()->first();
                if ($multi) {
                    ++$counter;
                    $this->fields[$field.' surname '.$counter]        = $f->last_name;
                    $this->fields[$field.' name '.$counter]           = $f->first_name;
                    $this->fields[$field.' dob day '.$counter]        = (string)$dob->day;
                    $this->fields[$field.' dob month '.$counter]      = (string)$dob->month;
                    $this->fields[$field.' dob year '.$counter]       = (string)$dob->year;
                    $this->fields[$field.' place of birth '.$counter] = $f->place_of_birth;
                    $this->fields[$field.' citizenship '.$counter]    = $f->citizenship;
                    $this->fields[$field.' address '.$counter]        = ($address) ? $address->street1.' '.$address->street2.', '.$address->town.', '.$address->country.', '.$address->postal_code : 'N/A';
                    $this->fields[$field.' occupation '.$counter]     = ($hasProfile) ? $f->Work_or_businesses()->first()->primary_occupation : $f->occupation;
                    $this->fields[$field.' included no '.$counter] = $field.' included no';
                } else {
                    $this->fields[$field.' surname']        = $f->last_name;
                    $this->fields[$field.' name']           = $f->first_name;
                    $this->fields[$field.' dob day']        = (string)$dob->day;
                    $this->fields[$field.' dob month']      = (string)$dob->month;
                    $this->fields[$field.' dob year']       = (string)$dob->year;
                    $this->fields[$field.' place of birth'] = $f->place_of_birth;
                    $this->fields[$field.' citizenship']    = $f->citizenship;
                    $this->fields[$field.' address']        = ($address) ? $address->street1.' '.$address->street2.', '.$address->town.', '.$address->country.', '.$address->postal_code : 'N/A';
                    $this->fields[$field.' occupation']     = ($hasProfile) ? $f->Work_or_businesses()->first()->primary_occupation : $f->occupation;
                    $this->fields[$field.' included no'] = $field.' included no';
                }

            }
        }
    }

    protected function spouse() {
        if ($this->profile->belongs_to === 'Child' || $this->profile->belongs_to === 'Mother' || $this->profile->belongs_to === 'Father') {
            return "";
        }
        $hasProfile = $this->user->profiles()->where('belongs_to', 'Spouse')->count();
        $spouse     = NULL;
        if ($hasProfile) {
            $spouse = $this->user->profiles()->where('belongs_to', 'Spouse')->first();
        } else {
            $spouse = $this->profile->family_members()->where('relationship', 'Spouse')->first();
        }
        if ($spouse) {
            $this->fields['Spouse Name Maiden']     = $spouse->first_name.' '.$spouse->last_name;
            $this->fields['Spouse Place of birth']  = $spouse->place_of_birth;
            $this->fields['Spouse Nationality']     = ($hasProfile) ? $spouse->citizenship_at_birth : $spouse->citizenship;
            $this->fields['Spouse Passport No']     = ($hasProfile) ? $this->getProfilePassport($spouse)->document_number : 'N/A';
            $this->fields['Spouse landline']        = ($hasProfile) ? $spouse->personal_landline_no : 'N/A';
            $this->fields['Spouse work no']         = ($hasProfile) ? $spouse->personal_mobile_no : 'N/A';
            $this->spouseAddr($spouse);
            $this->spouseEmployment($spouse);
        }
    }

    protected function exSpouse() {
        $ex = $this->profile->previous_spouses()->get();
        if ($ex) {
            $counter = 0;
            foreach ($ex as $e) {
                ++$counter;
                $this->fields['ex spouse name '.$counter]           = $e->full_name;
                $this->fields['ex spouse place of birth '.$counter] = $e->place_of_birth;
                $this->fields['ex spouse nationality '.$counter]    = $e->nationality;
                $this->fields['Date of Divorce '.$counter]          = $e->divorse_dt;
                $this->fields['Period of Marriage '.$counter]       = $e->marriage_from_dt;
            }
        }
    }

    protected function spouseEmployment($spouse) {
        $emp = $spouse->work_or_businesses()->first();
        $empAdd = Address::find($emp->address_id);
        $this->fields['Spouse occupation']          = $emp->primary_occupation;
        $this->fields['Spouses Employer']           = $emp->name_of_business;
        $this->fields['Spouse Employer Address']    = $empAdd->street1.' '.$empAdd->street2;
        $this->fields['Spouse Employer city']       = $empAdd->town;
        $this->fields['Spouse Employer country']    = $empAdd->country;
        $this->fields['Spouse Employer zip']        = $empAdd->postal_code;
    }

    protected function spouseAddr($spouse) {
        $pa     = $this->user->profiles()->where('belongs_to', 'Principal Applicant')->first();
        $paAddr = $pa->addresses()->where('address_type', 'cur')->first();
        $spAddr = $spouse->addresses()->where('address_type', 'cur')->first();
        if ($spAddr && ($spAddr->street1 !== $paAddr->street1)) {
            $this->fields['Spouse Address']             = $spAddr->street1.' '.$spAddr->street2;
            $this->fields['Spouse Address city']        = $spAddr->town;
            $this->fields['Spouse Address country']     = $spAddr->country;
            $this->fields['Spouse Address postal code'] = $spAddr->postal_code;
        }
    }

    protected function referenes() {
        $refs       = $this->profile->employment_references()->get();
        $counter    = 0;
        foreach ($refs as $ref) {
            ++$counter;
            $this->fields['Employment Start '.$counter]                 = $ref->from_date;
            $this->fields['Employment End '.$counter]                   = $ref->to_date;
            $this->fields['Name of Employer '.$counter]                 = $ref->employer;
            $this->fields['Employment Address and Phone No '.$counter]  = $ref->location;
            $this->fields['Position held '.$counter]                    = $ref->occupation;
            $this->fields['Industry '.$counter]                         = $ref->type_of_business;
            $this->fields['Reasons for leaving '.$counter]              = $ref->reason_for_leaving;
        }
    }

    protected function schools() {
        $schools    = $this->profile->qualifications()->get();
        $counter    = 0;
        foreach ($schools as $school) {
            ++$counter;
            $this->fields['School Peroid Start '.$counter]  = $school->from_date;
            $this->fields['School Peroid End '.$counter]    = $school->to_date;
            $this->fields['Name of school '.$counter]       = $school->qualification_name;
            $this->fields['School Address '.$counter]       = $school->location;
            $this->fields['Qualification '.$counter]        = $school->qualification_type;
        }
    }

    // FIXME: Totals not calculating correctly.
    protected function assetsLibs() {
        $asLibs     = $this->profile->liable_assets()->get();
        $totAssets  = 0.0;
        $totLibs    = 0.0;
        foreach ($asLibs as $value) {
            $spl = [];
            if ($value->item === 'Fixed Assets') {
                $this->fields['fixed assets'] = $value->amount;
                $totAssets = number_format($totAssets + str_replace(",", ".",$value->amount));
            } else if ($value->item === 'Savings / Deposits') {
                $this->fields['savings'] = $value->amount;
                $totAssets = number_format($totAssets + str_replace(",", ".",$value->amount));
            } else if ($value->item === 'Investments') {
                $this->fields['investments'] = $value->amount;
                $totAssets = number_format($totAssets + str_replace(",", ".",$value->amount));
            } else if (strpos($value->item, 'Other -') !== false && $value->indicator = 1) {
                $total = (!empty(explode('-',$value->amount)[1])) ? trim(explode('-',$value->amount)[1]) : 'N/A';
                $this->fields['other assets'] = ($total !== 'N/A') ? number_format($totAssets + str_replace(",", ".",$total)) : $total;
                $totAssets = number_format($totAssets + str_replace(",", ".",$total));
            } else if ($value->item === 'Outstanding Long Term Loans') {
                $this->fields['lib outstanding long']   =  $value->amount;
                $totLibs = number_format($totLibs + str_replace(",", ".",$value->amount));
            } else if ($value->item === 'Outstanding Short Term Loans') {
                $this->fields['lib outstanding short']  = $value->amount;
                $totLibs = number_format($totLibs + str_replace(",", ".",$value->amount));
            } else if (strpos($value->item, 'Other -') !== false && $value->indicator = 2) {
                $total = (!empty(explode('-',$value->amount)[1])) ? trim(explode('-',$value->amount)[1]) : 'N/A';
                $this->fields['lib other'] = $total;
                $totLibs = ($total !== 'N/A') ? number_format($totLibs + str_replace(",", ".",$total)) : $total;
                $this->fields['lib other'] = $total;
            }
        }

        $this->fields['assets total'] = (string) number_format($totAssets, 3);
        $this->fields['lib total'] = (string) number_format($totLibs, 3);
    }

    protected function licenses($business) {
        $lic = $business->professional_licences()->get();
        $counter = 0;
        if ($lic->count() > 0) {
            foreach ($lic as $l) {
                ++$counter;
                $this->fields['designation held '.$counter] = $l->licence_type;
                $this->fields['practice number '.$counter]  = $l->licence_no;
                $this->fields['authority '.$counter]        = $l->authority;
            }
        }
    }

    protected function businessAddr($business) {
        $address = Address::find($business->address_id);
        return $address->street1.' '.$address->street2.', '.$address->country.', '.$address->town.', '.$address->postal_code;
    }

    // A36 Mapping
    protected function otherAddr() {
        $addresses  = $this->profile->addresses()->where('address_type', 'ten')->get();
        $counter    = 0;
        foreach ($addresses as $addr) {
            ++$counter;
            $this->fields['address date from '.$counter]    = $addr->pivot->from_date;
            $this->fields['address date to '.$counter]      = $addr->pivot->from_date;
            if (!empty($addr->steet2)) {
                $this->fields['address date address '.$counter] = $addr->street1.', '.$addr->steet2.', '.$addr->town.', '.$addr->country.', '.$addr->postal_code;
            } else {
                $this->fields['address date address '.$counter] = $addr->street1.', '.$addr->town.', '.$addr->country.', '.$addr->postal_code;
            }
        }
    }

    protected function military() {
        $military = $this->profile->military_information;

        if ($military) {
            $this->fields['millitary yes'] = 'millitary yes';
            $doe = Carbon::parse($military->active_service_dt);
            $dos = Carbon::parse($military->seperation_dt);
            $this->fields['millitary branch']           = $military->branch;
            $this->fields['date of entry day']          = (string)$doe->day;
            $this->fields['date of entry month']        = (string)$doe->month;
            $this->fields['date of entry year']         = (string)$doe->year;
            $this->fields['date of seperation day']     = (string)$dos->day;
            $this->fields['date of seperation month']   = (string)$dos->month;
            $this->fields['date of seperation year']    = (string)$dos->year;
            $this->fields['type of discharge']          = $military->type_of_discharge;
            $this->fields['ranking']                    = $military->ranking_at_seperation;
            $this->fields['serial no']                  = $military->serial_no;
        } else {
            $this->fields['millitary no'] = 'millitary no';
        }

    }

    protected function postalAddr() {
        $ddr = $this->profile->addresses()->where('address_type', 'res')->first();
        if ($ddr) {
            $this->fields['postal addrees']                    = $ddr->street1.''.($ddr->street2) ? ', '.$ddr->street2 : '';
            $this->fields['postal addrees city']               = $ddr->town;
            $this->fields['postal addrees country']            = $ddr->country;
            $this->fields['postal addrees zip code']           = $ddr->postal_code;
            $this->fields['postal addrees date since month']   = ($ddr->date_since) ? explode('/', $ddr->date_since)[0] : '';
            $this->fields['postal addrees date since year']    = ($ddr->date_since) ? explode('/', $ddr->date_since)[1] : '';
        }
    }

    /**
     * A16 Mapper Function. Maps all Dep.
     */
    protected function includedMembers() {
        $profiles   = $this->user->profiles()->get()->sortBy('created_at');
        $idx        = 0;
        foreach ($profiles as $i => $p) {
            if ($p->profile_id === $this->profile->profile_id) {
                continue;
            }
            ++$idx;
            $this->fields['included name '.$idx]            = $p->first_name.' '.$p->middle_names.' '.$p->last_name;
            $this->fields['included dob '.$idx]             = Carbon::parse($p->date_of_birth)->format('d/m/Y');
            $this->fields['included nationality '.$idx]     = $p->country_of_birth;
            $this->fields['included passport no '.$idx]     = $this->getProfilePassport($p)->document_number;
            $this->fields['included relationship '.$idx]    = $this->belongdTo($p);
        }
    }

    protected function belongdTo($p) {
        $return = $p->belongs_to;
        if ($this->profile->belongs_to === "Spouse" && $p->belongs_to === "Principal Applicant") {
            $return = "Spouse";
        } else if ($this->profile->belongs_to === "Child") {
            if ($p->belongs_to === "Spouse" || $p->belongs_to === "Principal Applicant") {
                $return = "Parent";
            } else if ($p->belongs_to === "Child") {
                $return = "Sibling";
            } else if ($p->belongs_to === "Father" || $p->belongs_to === "Mother") {
                $return = "Grandparent";
            }
        } else if ($this->profile->belongs_to === "Father" || $this->profile->belongs_to === "Mother") {
            if ($p->belongs_to === "Spouse" || $p->belongs_to === "Principal Applicant") {
                $return = "Child";
            } else if ($p->belongs_to === "Child") {
                $return = "Grandchild";
            }
        }
        return $return;
    }

    protected function sepPassports() {
        $passports = $this->profile->identity_documents()->where('document_type', 'passport')->get();
        foreach ($passports as $key => $passport) {
            $idx = $key + 1;
            $this->fields['passport no '.$idx]          =  $passport->document_number;
            $this->fields['passport country '.$idx]     =  $passport->issuing_country;
            $this->fields['passport date issued '.$idx] =  $passport->date_of_issue;
            $this->fields['passport date exp '.$idx]    =  $passport->date_of_expiration;
        }
    }

    protected function setCard($type, $withCounter = false) {
        if ($withCounter) {
            $card = $this->profile->identity_documents()->where('document_type', $type)->get();
            foreach ($card as $key => $c) {
                $idx = $key + 1;
                $this->fields[$type.' no '.$idx]        = $c->document_number;
                $this->fields[$type.' country '.$idx]   = $c->issuing_country;
            }

        } else {
            $card = $this->profile->identity_documents()->where('document_type', $type)->first();
            if ($card) {
                $this->fields[$type.' no']      = $card->document_number;
                $this->fields[$type.' country'] = $card->issuing_country;
            } else {
                $this->fields[$type.' no']      = 'N/A';
                $this->fields[$type.' country'] = 'N/A';
            }

        }
    }

    protected function decodeLang($langs) {
        $return = NULL;
        foreach (explode(',', $langs) as $l) {
            $return = $return.' '.LanguageType::find($l)->type;
        }
        return $return;
    }

}
