<?php

namespace hpsadev\ProgramsFileManager\Mappers;

use hpsadev\ProgramsFileManager\Transforms\D1Transform;

/**
 * Dominica D1 Mapper
 *
 * @author      Gershon Koks <gershon.koks@henleyglobal.com>
 * @copyright   Henley & Partners Ltd
 * @license     For Henley & Partners use only.
 * @version     1.1
 */
class D1Mapper extends Mapper
{

    use D1Transform;

    function __construct($profile)
    {
        $this->profile  = $profile;
        $this->user     = $this->profile->user;
        $this->app      = $this->profile->application;
        $this->mapper();
    }

    protected function mapper() {
        $this->fields['family name']    = $this->profile->last_name;
        $this->fields['names']          = $this->profile->first_name.' '.$this->profile->middle_names;
        $this->fields['passport no']    = $this->getPassport()->document_number;
        $this->fields['country of issue'] = $this->getPassport()->issuing_country;
        $this->fields['date of birth']  = $this->profile->date_of_birth;
        $this->partA();
        $this->partB();
        $this->partC();
        $this->partD();
    }

}
