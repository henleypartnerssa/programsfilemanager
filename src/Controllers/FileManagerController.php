<?php

namespace hpsadev\ProgramsFileManager\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Util\AppPdfHandler;
use hpsadev\ProgramsFileManager\DirectoryManager;
use hpsadev\ProgramsFileManager\FileManager;
use hpsadev\ProgramsFileManager\PdfBuilder;
use App\Application;
use App\Profile;
use App\Address;
use App\FamilyMember;
use Auth;
use Input;

/**
 * File Manager Controller.
 *
 * @author      Gershon Koks <gershon.koks@henleyglobal.com>
 * @copyright   Henley & Partners Ltd
 * @license     For Henley & Partners use only.
 * @version     0.0.2
 */
class FileManagerController extends Controller
{

    public function __construct() {
        // executes no pin middleware
        $this->middleware('nopin');
    }

    public function generateGovForm(Request $request) {
        $builder    = new PdfBuilder($request['form'], Profile::find($request['profile_id']));
        $generated  = $builder->build();
        if ($generated == '1') {
            return json_encode([
                'state' => '1',
                'file'  => $builder->getOutputFileName()
            ]);
        } else {
            return json_encode([
                'state' => '0',
                'file'  => $builder->getOutputFileName()
            ]);
        }
    }

    public function uploadToProfilePayments(Request $request) {
        $fm = new FileManager($request['profile_id']);
        return $fm->toProfilePayments($request);
    }

    public function uploadToProfileReceipts(Request $request) {
        $fm = new FileManager($request['profile_id']);
        return $fm->toProfileReceipts($request);
    }

    public function uploadToProfileUploads(Request $request) {
        $fm = new FileManager($request['profile_id']);
        return $fm->toProfileUploads($request);
    }


    public function uploadToProfileNotary(Request $request) {
        $fm = new FileManager($request['profile_id']);
        return $fm->toProfileNotary($request);
    }

    public function downloadGovForm($fileName) {
        $fm = new FileManager();
        return $fm->fromDownloads($fileName);
    }

    public function downloadFromUploads($fileName) {
        $fm = new FileManager();
        return $fm->fromUploads($fileName);
    }

    public function downloadFromNotary($fileName) {
        $fm = new FileManager();
        return $fm->fromNotary($fileName);
    }

    public function downloadFromPayments($fileName) {
        $fm = new FileManager();
        return $fm->fromPayments($fileName);
    }

    public function downloadFromReceipts($fileName) {
        $fm = new FileManager();
        return $fm->fromReceipts($fileName);
    }

    public function deleteGovForm(Request $request) {
        $fm = new FileManager($request['profile_id']);
        return $fm->delFromDownloads($request);
    }

    public function deleteFromUploads(Request $request) {
        $fm = new FileManager($request['profile_id']);
        return $fm->delFromUploads($request);
    }

    public function deleteFromNotary(Request $request) {
        $fm = new FileManager($request['profile_id']);
        return $fm->delFromNotary($request);
    }

    public function deleteFromPayments(Request $request) {
        $fm = new FileManager($request['profile_id']);
        return $fm->delFromPayments($request);
    }

    public function deleteFromReceipts(Request $request) {
        $fm = new FileManager($request['profile_id']);
        return $fm->delFromReceipts($request);
    }

    public function listDocumentes($key) {
        $profile_id = explode("|", $key)[0];
        $fm         = new FileManager($profile_id);
        $files      = $fm->getAllUploads($key);
        $authUser   = Auth::user();
        $profile    = Profile::find($profile_id);
        $user       = $profile->user;
        $folder     = (!empty(explode("|", $key)[1])) ? explode("|", $key)[1] : 'uploads';
        $paId       = $user->profiles()->where('belongs_to', 'Principal Applicant')->first()->profile_id;
        $view       = ($authUser->group->roles === 'admin') ? 'admin' : 'users';
        return view('programsfilemanager::'.$view.'_documents', compact('files', 'profile_id', 'paId', 'folder', 'user', 'profile'));
    }

}
