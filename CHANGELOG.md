# Changelog

All Notable changes to `hpsadev/ProgramsFileManager` will be documented in this file.

## [0.0.2] 2016-05-20
### Added
- Nothing

## [0.0.1] 2016-05-20
### Added
- Project Init

### Deprecated
- Nothing

### Fixed
- Nothing

### Removed
- Nothing

### Security
- Nothing
