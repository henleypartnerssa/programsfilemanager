<?php

namespace hpsadev\ProgramsFileManager\Mappers;

use hpsadev\ProgramsFileManager\Transforms\D3Transform;

/**
 * Dominica D3 Mapper
 *
 * @author      Gershon Koks <gershon.koks@henleyglobal.com>
 * @copyright   Henley & Partners Ltd
 * @license     For Henley & Partners use only.
 * @version     1.1
 */
class D3Mapper extends Mapper {

    use D3Transform;

    function __construct($profile)
    {
        $this->profile  = $profile;
        $this->user     = $this->profile->user;
        $this->app      = $this->profile->application;
        $this->mapper();
    }

    protected function mapper() {
        $this->fields['fullname']               = $this->profile->first_name.' '.$this->profile->middle_names.' '.$this->profile->last_name;
        $this->fields['address']                = $this->addressStr();;
        $this->fields['country of residence']   = $this->address()->country;
        $this->fields['dob']                    = $this->profile->date_of_birth;
        $this->fields[$this->profile->gender]   = $this->profile->gender;
        $this->fields['passport no']            = $this->getPassport()->document_number;
        $this->fields['date and place']         = $this->getPassport()->date_of_issue.' - '.$this->getPassport()->place_of_issue;
        $this->fields['occupation']             = (!empty($this->getBusiness()->primary_occupation)) ? $this->getBusiness()->primary_occupation : 'N/A';
        $this->fields['marital status']         = ($this->profile->marital_status) ? $this->profile->marital_status : 'Single';
        $this->fields['height']                 = $this->profile->height;
        $this->fields['weight']                 = $this->profile->weight;
        $this->fields['email']                  = $this->profile->personal_email_address;
        $this->fields['medical fullname']       = $this->getMedical()->doctor_name;
        $this->fields['medical address']        = $this->getMedicalAddrStr();
        $this->fields['medical tel']            = $this->getMedical()->doc_tel;
        $this->fields['medical fax']            = $this->getMedical()->doc_fax;
        $this->fields['medical email']          = $this->getMedical()->doc_email;
        $this->a_one();
        $this->a_two();
        $this->a_three();
        $this->a_four();
    }

}
