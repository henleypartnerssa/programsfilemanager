<?php

namespace hpsadev\ProgramsFileManager\Mappers;

use Carbon\Carbon;

/**
 * Dominica D4 Mapper
 *
 * @author      Gershon Koks <gershon.koks@henleyglobal.com>
 * @copyright   Henley & Partners Ltd
 * @license     For Henley & Partners use only.
 * @version     1.1
 */
class D4Mapper extends Mapper
{

    function __construct($profile)
    {
        $this->profile  = $profile;
        $this->user     = $this->profile->user;
        $this->app      = $this->profile->application;
        $this->mapper();
        $this->dependants();
    }

    protected function mapper() {
        $this->fields['fullname']               = $this->profile->first_name.' '.$this->profile->middle_names.' '.$this->profile->last_name;
        $this->fields['address']                = $this->addressStr();
        $this->fields['country of residence']   = $this->address()->country;
        $this->fields['dob']                    = Carbon::parse($this->profile->date_of_birth)->format('d/m/Y');
        $this->fields['passport no']            = $this->getPassport()->document_number;
        $this->fields['date and place']         = $this->getPassport()->date_of_issue.' - '.$this->getPassport()->place_of_issue;
        $this->fields[$this->profile->gender]   = $this->profile->gender;
    }

    protected function dependants() {
        $counter = 0;
        foreach ($this->user->profiles()->get() as $profile) {
            if ($profile->belongs_to === 'Principal Applicant') {
                continue;
            }
            ++$counter;
            $this->fields['dep fullname '.$counter]     = $profile->first_name.' '.$profile->middle_names.' '.$profile->last_name;
            $this->fields['dep dob '.$counter]          = Carbon::parse($profile->date_of_birth)->format('d/m/Y');
            $this->fields['dep relationship '.$counter] = $profile->belongs_to;
        }
    }

}
